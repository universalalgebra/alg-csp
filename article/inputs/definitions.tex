
\subsection{Notation for projections, scopes, and kernels}
\label{sec:proj-scop-kern}
Here we give a somewhat informal description of some definitions and notations
we use in the sequel.  Some of these will be reintroduced more carefully later on, as needed.

An operation $f \colon A^n \rightarrow A$ is called \defn{idempotent} provided 
$f(a, a, \dots, a) = a$ for all $a \in A$.
Examples of idempotent operations are the projection functions and these will 
play an important role in later sections, so we start by 
introducing a sufficiently general and flexible notation for them.

We define the natural numbers as usual and denote them as follows:
\[
\uzero := \emptyset, \quad
\uone := \{0\}, \quad
\utwo := \{0, 1\}, %\dots, %% \]%% \[
\quad \dots \quad \nn := \{0, 1, 2, \dots, n-1\}, \quad\dots
\]
Given sets $A_0$, $A_1$, $\dots$, $A_{n-1}$, their Cartesian product is
%% \[\myprod_{\nn} A_i := A_0 \times A_1 \times \cdots \times A_{n-1}.\] 
$\myprod_{\nn} A_i := A_0 \times % A_1 \times 
\cdots \times A_{n-1}$.
An element
$\ba \in \myprod_{\nn} A_i$ is an ordered $n$-tuple, which may be specified by
simply listing its values, 
as in $\ba = (\ba(0), \ba(1), \dots, \ba(n-1))$.
Thus, tuples are functions defined on a (finite) index set, and
this view may be emphasized symbolically as follows:
\[
\ba \colon \nn \to \bigcup_{i\in \nn} A_i; \;\; i\mapsto \ba(i) \in A_i.
\]

If $\sigma\colon \kk \to \nn$ is a $k$-tuple of numbers in 
$\nn$, then we can compose an $n$-tuple $\ba$ in $\myprod_{i\in \nn} A_i$
with $\sigma$ yielding the $k$-tuple $\ba\circ \sigma$ in 
$\myprod_{\kk}A_{\sigma(i)}$.
Generally speaking, we will try to avoid nonstandard notational conventions, 
but here are two exceptions: let
\[
\uA := \prod_{i\in \nn} A_i \quad \text{ and } \quad \uA_{\sigma} := \prod_{i\in \kk} A_{\sigma(i)}.
\]

Now, if the $k$-tuple $\sigma\colon \kk \to \nn$ happens to be one-to-one,
and if we let $p_\sigma$ denote the map $\ba \mapsto \ba\circ \sigma$, then $p_\sigma$
is the usual \emph{projection function} from $\uA$ onto $\uA_{\sigma}$.
Thus, $p_{\sigma}(\ba)$ is a $k$-tuple whose $i$-th component is 
$(\ba\circ \sigma)(i) = \ba(\sigma(i))$.
%$\myprod_{\kk}A_{\sigma(i)}$.
We will make frequent use of such projections, as well as
their images under the covariant and contravariant powerset functors
%% \ifthenelse{\boolean{extralong}}{
%% $\sP$ and $\overbar{\sP}$. depicted in Figure \ref{fig:powersetfunctor}.
%% }{$\sP$ and $\overbar{\sP}$.}
$\sP$ and $\overbar{\sP}$.
  Indeed, we let
$\Proj_\sigma\colon \sP(\uA) \to \sP(\uA_\sigma)$
denote the \emph{projection set function} defined 
for each $R \subseteq \uA$ by
\[
\Proj_\sigma R = \sP(p_\sigma)(R) = \{p_\sigma(\bx) \mid \bx \in R\} = 
\{\bx\circ \sigma \mid \bx \in R\},
\]
and we let
$\Inj_\sigma\colon \overbar{\sP}(\uA_\sigma) \to \overbar{\sP}(\uA)$
denote the \emph{injection set function} defined for each
$S\subseteq \uA_\sigma$ by
\begin{equation}\label{eq:19}
  \Inj_\sigma S = \overbar{\sP}(p_\sigma)(S) = \{\bx \mid p_\sigma(\bx) \in S\}
   = \{\bx \in \uA \mid \bx\circ \sigma \in S\}.
\end{equation}
Of course, $\Inj_\sigma S$ is nothing more than the inverse image of the set $S$ with respect to the
projection function $p_\sigma$.
We sometimes use the shorthand $R_\sigma = \Proj_\sigma R$ and
$S^{\overleftarrow{\sigma}} = \Inj_\sigma S$ for the projection and injection set functions, respectively.

%% \ifthenelse{\boolean{extralong}}{%
%%   %%% --- wjd: (2016.10.02) omitting figure from arxiv and journal version
%%   \begin{figure}[!h]
%%     \centering
%%     \begin{tikzpicture}[node distance=2cm, auto, scale=2]
%%       \node (023) at (0,2.3)  {$\Set$};
%%       \node (123) at (1,2.3)  {$\Set$};
%%       \node (02) at (0,2)  {$\uA$};
%%       \node (12) at (1,2)  {$\sP (\uA)$};
%%       \node (01) at (0,1)  {$\uA_{\sigma}$};
%%       \node (11) at (1,1)  {$\sP(\uA_{\sigma})$};
%%       %% \node (middle) at (0.5,1.5)  {$\circlearrowleft$};
%%       \draw[->] (02) -- (01)  node[pos=.5,left] {$p_\sigma$};
%%       \draw[->] (12) -- (11)  node[pos=.5,right] {$\sP(p_\sigma)$};
%%       \draw[->] (023) -- (123)  node[pos=.5,above] {$\sP$};
%%     \end{tikzpicture}
%%     \hskip1cm
%%     \begin{tikzpicture}[node distance=2cm, auto, scale=2]
%%       \node (023) at (0,2.3)  {$\Set^{\op}$};
%%       \node (123) at (1,2.3)  {$\Set$};
%%       \node (02) at (0,2)  {$\uA_{\sigma}$};
%%       \node (11) at (1,1)  {$\overbar{\sP} (\uA)$};
%%       \node (01) at (0,1)  {$\uA$};
%%       \node (12) at (1,2)  {$\overbar{\sP}(\uA_{\sigma})$};
%%       \draw[->] (02) -- (01)  node[pos=.5,left] {$p_\sigma$};
%%       \draw[->] (12) -- (11)  node[pos=.5,right] {$\overbar{\sP}(p_\sigma)$};
%%       \draw[->] (023) -- (123)  node[pos=.5,above] {$\overbar{\sP}$};
%%     \end{tikzpicture}
%%     \caption{Projection under covariant (left) and contravariant (right) powerset functors.}
%%     \label{fig:powersetfunctor}
%%   \end{figure}
%% }{}
%%% ---
\ifthenelse{\boolean{extralong}}{%
  %%% --- wjd: (7/24/2016) omitting example from arxiv and journal version
  \begin{exa}
    To make clear why the term ``projection'' is reserved for the 
    case when $\sigma$ is one-to-one, suppose 
    $k=4$, $n=3$, and consider the 4-tuple $\sigma = (1, 0, 1, 1)$. 
    Then $\sigma$ is the function 
    $\sigma \colon \{0,1,2,3\} \to \{0,1,2\}$ given by 
    $\sigma(0) = 1$,
    $\sigma(1) = 0$, $\sigma(2) = 1$, $\sigma(3) = 1$, and so 
    $a \mapsto a\circ \sigma$ is the function that takes
    $(a_0, a_1, a_2)\in A_0 \times A_1 \times A_2$ to 
    $(a_1, a_0, a_1, a_1) \in A_1 \times A_0 \times A_1 \times A_1$.
  \end{exa}
}{}
%%% ---

A one-to-one function $\sigma \colon \kk \to \nn$ is sometimes called a \emph{scope} or
\emph{scope function}.
If $R \subseteq \prod_{j \in \kk}A_{\sigma(j)}$
is a subset of the projection of $\uA$ onto coordinates
$(\sigma(0), \sigma(1), \dots, \sigma(k-1))$,
then we call $R$ a \emph{relation on $\uA$ with scope $\sigma$}.
The pair $(\sigma, R)$ is called a \emph{constraint}, and
$R^{\overleftarrow{\sigma}}$
is the set of tuples in $\uA$ that \emph{satisfy}
$(\sigma, R)$.

By $\eta_\sigma$ we denote the \emph{kernel} of the projection function $p_\sigma$,
which is the following equivalence relation on $\uA$:
\begin{equation}
  \label{eq:60}
  \eta_\sigma 
  = \{(\ba,\ba') \in \uA^2 \mid p_\sigma(\ba) = p_\sigma(\ba') \}
  = \{(\ba,\ba') \in \uA^2 \mid \ba \circ \sigma = \ba' \circ \sigma \}.
\end{equation}

  More generally, if $\theta$ is an equivalence relation on the set
  $\myprod_{j\in \kk} A_{\sigma(j)}$, then we  define the equivalence relation $\theta_\sigma$ on
  the set $\uA = \myprod_{\nn} A_i$ as follows:
\begin{equation}
  \label{eq:17}
\theta_\sigma = 
\{(\ba, \ba') \in \uA^2 \mid (\ba\circ \sigma) \mathrel{\theta} (\ba' \circ \sigma)\}.
\end{equation}
In other words,  $\theta_\sigma$ consists of all pairs in $\uA^2$ that land in $\theta$
when projected onto the scope $\sigma$. Thus,
if $0_{\uA}$ denotes the least equivalence relation on {\uA},
then $\eta_\sigma$ is shorthand for $(0_{\uA})_\sigma$.


If the domain of $\sigma$ is a singleton, $\kk = \{0\}$, then of course
$\sigma$ is just a one-element list, say, $\sigma= (j)$. 
In such cases, we write $p_j$ instead of $p_{(j)}$. Similarly,
we write $\Proj_j$ instead of $\Proj_{(j)}$, $\eta_j$ instead of $\eta_{(j)}$, etc.  Thus, 
$p_j(\ba) = \ba(j)$, and $\eta_j = \{(\ba, \ba') \in \uA^2 \mid \ba(j) = \ba'(j)\}$,
and, if $\theta\in \Con \bA_j$, then
$\theta_j = \{(\ba, \ba') \in \uA^2 \mid \ba(j) \mathrel{\theta} \ba'(j)\}$.
Here are some obvious consequences of these definitions:
\[
\bigvee_{j\in \nn}\eta_j =\uA^2, \qquad
 \eta_\sigma= \bigwedge_{j\in \sigma}\eta_j, \qquad %\quad \text{ and } \quad
 \eta_{\nn} = \bigwedge_{j\in \nn}\eta_j = 0_{\uA}, \qquad
\theta_\sigma = \bigwedge_{j\in \kk}\theta_{\sigma(j)}.
\]
%% where $0_{\uA}$ denotes the least equivalence relation on {\uA}.
%% , that is, $0_{\uA}:= \{(\ba, \ba') \in \uA^2 \mid \ba = \ba'\}$.  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Notation for algebraic structures}
This section introduces the notation we use for algebras and related concepts.
The reader should consult~\cite{MR2839398} for more details and background 
on general (universal) algebras and the varieties they inhabit.

\subsubsection{Product algebras}
Fix $n\in \N$, let $F$ be a set of operation symbols, and for each $i\in \nn$
let $\bA_i = \<A_i, F\>$ be an algebra of type $F$.
Let $\ubA = \bA_0 \times \bA_1 \times \cdots \times \bA_{n-1}= \myprod_{\nn} \bA_i$
denote the product algebra.
If $k \leq n$ and if $\sigma \colon \kk\to \nn$ is a one-to-one function, then
we denote by
$p_\sigma \colon \ubA \onto \myprod_{\kk}\bA_{\sigma(i)}$
the \defn{projection} of $\ubA$ onto the 
``$\sigma$-factors'' % (factors with indices in $\im \sigma$)
of $\ubA$,
which is an algebra homomorphism; thus the kernel $\eta_\sigma$ defined
in~(\ref{eq:60}) is a congruence relation of $\ubA$.   


\subsubsection{Term operations}
\label{ssec:term-ops}
For a nonempty set $A$, we let $\sansO_A$ denote the set of all finitary
operations on $A$. That is, $\sansO_A = \bigcup_{n\in \N}\AAn$.
A \defn{clone of operations} on $A$ is a subset of $\sansO_A$ that contains all
projection operations and is closed under the (partial) operation of general
composition. 
If $\bA = \<A, F^\bA\>$ denotes the algebra with universe $A$ and set of basic
operations $F$, then $\sansClo (\bA)$ denotes the clone generated by
$F$, which is also known as the \defn{clone of term operations} of $\bA$.

Walter Taylor proved in~\cite{MR0434928} that a variety, $\var{V}$, satisfies some
nontrivial idempotent \malcev condition if and only if it satisfies one of the following
form: for some~$n$, $\var{V}$ has an idempotent $n$-ary term  $t$ such that
for each $i\in \nn$ there is an identity of the form
\[
t(\ast, \cdots, \ast, x, \ast, \cdots, \ast) \approx t(\ast, \cdots, \ast, y,
\ast, \cdots, \ast)
\]
true in $\var{V}$ where distinct variables $x$ and $y$ appear in the
$i$-th position on either side of the identity.  Such a term $t$ is now commonly
called a \defn{Taylor term}. 


Throughout this paper we assume all algebras are finite 
(though some results may apply more generally).
Starting in Section~\ref{sec:tayl-vari-rect},
we make the additional assumption that the algebras in
question come from a single \defn{Taylor variety} $\var{V}$, by which we mean that $\var{V}$
has a Taylor term and every term of $\var{V}$ is idempotent.


\subsubsection{Subdirect products}
If $k, n \in \N$, if $\sA = (A_0, A_1, \dots, A_{n-1})$ is a list
of sets, and if $\sigma \colon \kk \to \nn$ is a $k$-tuple, 
then a relation $R$ over $\sA$ with scope $\sigma$ is
a subset of the Cartesian product
$A_{\sigma(0)} \times A_{\sigma(1)} \times \cdots \times A_{\sigma(k-1)}$.
\ifthenelse{\boolean{extralong}}{%
  % wjd 2016.10.02: omitting from arxiv and journal version
  If $k, m\in \N$ and $R\subseteq \prod_{\kk}A_{\sigma(i)}$, and if
  for each $j \in \kk$ we have $\bu_j \in A_{\sigma(j)}^m$,
  then we write $(\bu_0, \bu_1, \dots, \bu_{k-1})\in \ubar{R}$ 
  precisely when $(\bu_0(i), \bu_1(i), \dots, \bu_{k-1}(i))\in R$ 
  for all $i\in \mm$.
}{}
Let $F$ be a set of operation symbols and for each $i\in \nn$
let $\bA_i = \<A_i, F\>$ be an algebra of type $F$.
If $\ubA = \myprod_{\nn}\bA_i$
is the product of these algebras, then a relation 
$R$ over $\sA$ with scope $\sigma$ is called 
\defn{compatible with $\ubA$} if 
it is closed under the basic operations in $F$.
In other words, $R$ is compatible if the induced algebra $\bR= \<R,F\>$ 
is a subalgebra of $\myprod_{\kk} \bA_{\sigma(j)}$.
If $R$ is compatible with the product algebra
and if the projection of $R$ onto each factor is surjective,
then $\bR$ is
called a \defn{subdirect product} of the algebras
in the list
$(\bA_{\sigma(0)}, \bA_{\sigma(1)}, \dots, \bA_{\sigma(k-1)})$; 
we denote this situation by writing
\ifthenelse{\boolean{extralong}}{%
  % wjd 2016.11.08: omitting from arxiv and journal version
  $\bR \sdp \myprod_{j\in \kk} \bA_{\sigma(j)}$.\footnote{Note 
    that even in the special case when $\Proj_j\bR = \bA_{\sigma(j)}$ for each
    $j\in \kk$ so that $\bR \sdp \myprod_{j\in \kk} \bA_{\sigma(j)}$, we 
    refrain from using $\Proj_\sigma \ubA$ to denote $\myprod_{j\in \kk} \bA_{\sigma(j)}$
    for the simple reason that $\sigma$ might not be one-to-one.  
    For example, we could have
    $\ubA = \bA_0 \times \bA_1$ and $\sigma = (1,0,1)$, in which case
    $\myprod_{j\in \kk} \bA_{\sigma(j)} = \bA_1 \times \bA_0 \times \bA_1$ and this is
    not the projection of $\ubA$ onto a subset of its factors.}
}{$\bR \sdp \myprod_{\kk} \bA_{\sigma(j)}$.}
